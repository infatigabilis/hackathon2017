pragma solidity ^0.4.0;

contract Registry {
    address public owner;
    string public name;

    address[] public performerStorage;
    OperationType[] public operationTypes;

    mapping (address => Performer) public performers;

    struct Performer {
        string name;
        uint8 rating;
        string desc;
    }

    struct OperationType {
        string name;
        string desc;
    }

    function Registry(string _name) {
        owner = msg.sender;
        name = _name;

        operationTypes.push(OperationType("Смена владельца авто", "!!!"));
    }

    function addPerformer(address performer, string name, uint8 rating, string desc) isOwner {
        if (rating == 0) throw;

        performerStorage.push(performer);
        performers[performer] = Performer(name, rating, desc);
    }

    function addOperationType(string name, string desc) isOwner {
        if (bytes(desc).length == 0) throw;

        operationTypes.push(OperationType(name, desc));
    }

    function getOperationTypesLength() constant returns(uint) {
        return operationTypes.length;
    }

    function verifyOperation(address performer, uint32 operation) constant returns(bool) {
        return (
            performers[performer].rating != 0) &&
            (bytes(operationTypes[operation].desc).length != 0 &&
            operation != 0
            );
    }

    function touch() isOwner constant returns(bool) {
        return true;
    }

    modifier isOwner() {
        if (msg.sender != owner) throw;
        _;
    }
}
