import React from 'react';
import config from '../config';
import Paper from 'material-ui/Paper';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Subheader from 'material-ui/Subheader';
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import Web3 from 'web3';

const styles = {
  paper: {
    width: '90%',
    margin: 'auto',
    marginTop: 40,
  },
  feild: {
    width: '90%',
    margin: 'auto',
    marginTop: 40,
    padding: '15px 25px 15px 25px',
  }
};

class Informer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: '',
      inputed: false,
      snackbar: false,
    }
  }

  handleChange = (event) => {
    this.setState({
      address: event.target.value,
    });
  };

  handleSubmit = () => {
    this.setState({
      inputed: true,
      snackbar: true,
    });
  }

  handleRequestClose = () => {
    this.setState({
      inputed: false,
    });
  };

  render() {
    if (this.state.inputed) {
      var web3 = new Web3(new Web3.providers.HttpProvider(config.host));
      var Auto = web3.eth.contract(config.autoAbi).at(this.state.address);

      try {
        Auto.owner();
      } catch(e) {
        return (
          <div>
            <Paper style={styles.feild} zDepth={2}>
              <div style={{width: '91%', display: 'inline-block'}}>
                <TextField
                  hintText="Введите адрес смарт-контракта"
                  fullWidth={true}
                  id="text-field"
                  value={this.state.address}
                  onChange={this.handleChange}
                />
              </div>
              <RaisedButton label="Отправить" primary={true} onClick={this.handleSubmit} style={{width: '8%', marginLeft: '1%'}} />
            </Paper>

            <Snackbar
              open={true}
              message="Вы ввели несущесвующий адрес!"
              autoHideDuration={1000}
              onRequestClose={this.handleRequestClose}
            />
          </div>
        )
      }

      var hidden = Auto.hidden();
      console.log(hidden);

      if(!hidden) {
        var l = Auto.getOperationsLength().toNumber();
        console.log(l);

        var data = [];
        for (var i = (l-1); i >= 0; i--) {
          var o = Auto.operations(i);
          o[0] = o[0].toNumber();
          o[4] = o[4].toNumber();
          var r = web3.eth.contract(config.registryAbi).at(o[1]);
          o[5] = r.name();
          o[6] = r.operationTypes(o[0]);
          var p = r.performers(o[2]);
          p[1] = p[1].toNumber();
          o[7] = p;

          console.log(o);
          data.push(o);
        }
      }
    }

    return (
      <div>
        <Paper style={styles.feild} zDepth={2}>
          <div style={{width: '91%', display: 'inline-block'}}>
            <TextField
              hintText="Введите адрес смарт-контракта"
              fullWidth={true}
              id="text-field"
              value={this.state.address}
              onChange={this.handleChange}
            />
          </div>
          <RaisedButton label="Отправить" primary={true} onClick={this.handleSubmit} style={{width: '8%', marginLeft: '1%'}} />
        </Paper>

        {this.state.inputed ?
        <Paper style={styles.paper} zDepth={2}>
          <Subheader>Информация</Subheader>
          <Table>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn>Владелец</TableHeaderColumn>
                <TableHeaderColumn>Идентификатор авто</TableHeaderColumn>
                <TableHeaderColumn>История обслуживания скрыта</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow>
                <TableRowColumn>{Auto.owner()}</TableRowColumn>
                <TableRowColumn>{Auto.id()}</TableRowColumn>
                <TableRowColumn>{Auto.hidden() ? 'Да' : 'Нет'}</TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
        : false}

        {this.state.inputed && !hidden ?
        <Paper style={styles.paper} zDepth={2}>
          <Subheader>История обслуживания</Subheader>
          <Table>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn>Реестр</TableHeaderColumn>
                <TableHeaderColumn>Исполнитель</TableHeaderColumn>
                <TableHeaderColumn>Рейтинг исполнителя</TableHeaderColumn>
                <TableHeaderColumn>Услуга</TableHeaderColumn>
                <TableHeaderColumn>Примечание</TableHeaderColumn>
                <TableHeaderColumn>Дата</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {data.map(i =>
                <TableRow selected={i[0] == 0 ? true : false}>
                  <TableRowColumn>{i[5]}</TableRowColumn>
                  <TableRowColumn>{i[0] == 0 ? i[2] : i[7][0]}</TableRowColumn>
                  <TableRowColumn>{i[0] == 0 ? '-' : i[7][1]}</TableRowColumn>
                  <TableRowColumn>{i[6][0]}</TableRowColumn>
                  <TableRowColumn>{i[3]}</TableRowColumn>
                  <TableRowColumn>{(new Date(i[4] * 1000)).toISOString().substring(0, 10)}</TableRowColumn>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Paper>
        : false}
      </div>
    );
  }
}

export default Informer;
