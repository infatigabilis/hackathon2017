import config from '../config';

export default (web3, registry, id, address, callback) => {
  var Registry = web3.eth.contract(config.autoAbi).new(
    registry,
    id,
   {
     from: address,
     data: config.autoData,
     gas: config.gas
   }, function (e, contract) {
     if (contract.address != undefined) callback(contract.address);
  });
}
