import config from '../config';

export default (web3, r1, r2, a) => {
  var Registry1 = web3.eth.contract(config.registryAbi).at(r1);
  var Registry2 = web3.eth.contract(config.registryAbi).at(r2);
  var Auto = web3.eth.contract(config.autoAbi).at(a);

  console.log(Registry1.touch({from: web3.eth.accounts[0]}));
  console.log(Registry1.owner());
  console.log(Registry1.name());
  var l = Registry1.getOperationTypesLength();
  for (var i = (l - 1); i >= 0; i--) console.log(Registry1.operationTypes(i));
  var ps = Registry1.getPerformerStorage();
  console.log(ps);
  ps.forEach(i => {
    var p = Registry1.performers(i);
    p[1] = p[1].toNumber();
    console.log(p);
  })

  console.log(Registry2.touch({from: web3.eth.accounts[1]}));
  console.log(Registry2.owner());
  console.log(Registry2.name());
  var l = Registry2.getOperationTypesLength();
  for (var i = (l - 1); i >= 0; i--) console.log(Registry2.operationTypes(i));
  var ps = Registry2.getPerformerStorage();
  console.log(ps);
  ps.forEach(i => {
    var p = Registry2.performers(i);
    p[1] = p[1].toNumber();
    console.log(p);
  })

  console.log(Registry2.touch({from: web3.eth.accounts[1]}));
  console.log(Registry2.owner());
  console.log(Registry2.name());
  var l = Registry2.getOperationTypesLength();
  for (var i = (l - 1); i >= 0; i--) console.log(Registry2.operationTypes(i));
  var ps = Registry2.getPerformerStorage();
  console.log(ps);
  ps.forEach(i => {
    var p = Registry2.performers(i);
    p[1] = p[1].toNumber();
    console.log(p);
  })

  console.log(Auto.touch({from: web3.eth.accounts[7]}));
  console.log(Auto.owner());
  console.log(Auto.registry());
  console.log(Auto.hidden());

  var l = Auto.getOperationsLength().toNumber();
  console.log(l);

  // Auto.makePrivate({from: web3.eth.accounts[7], gas: 500000});

  for (var i = (l-1); i >= 0; i--) {
    var o = Auto.operations(i);
    o[0] = o[0].toNumber();
    o[4] = o[4].toNumber();
    var r = web3.eth.contract(config.registryAbi).at(o[1]);
    o[5] = r.name();
    o[6] = r.operationTypes(o[0]);
    var p = r.performers(o[2]);
    // console.log(p);
    p[1] = p[1].toNumber();
    o[7] = p;
    console.log(o);
  }
}
