import config from '../config';

export default (web3) => {
  var Registry = web3.eth.contract(config.registryAbi).at(config.registryAddress);
  var Auto = web3.eth.contract(config.autoAbi).at(config.autoAddress);

  init(web3, (r1, r2, a) => {
    manageRegistry(r1);
  })
}
