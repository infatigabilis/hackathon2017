import config from '../config';

export default (web3, a1, a2, r2) => {
  var Auto = web3.eth.contract(config.autoAbi).at(a1);

  Auto.makeOperation(1, "Без происшествий", {from: web3.eth.accounts[4], gas: 800000});
  Auto.makeOperation(2, "", {from: web3.eth.accounts[4], gas: 800000});
  Auto.makeOperation(1, "Ужасный клиент!!! Советую не обслуживать!", {from: web3.eth.accounts[5], gas: 800000});

  Auto.changeOwner(web3.eth.accounts[7], {from: web3.eth.accounts[2], gas: 800000});

  Auto.makeOperation(1, "Бла бла бла", {from: web3.eth.accounts[4], gas: 500000});

  Auto.setRegistry(r2, {from: web3.eth.accounts[7], gas: 800000});

  Auto.makeOperation(1, "", {from: web3.eth.accounts[5], gas: 800000});
  Auto.makeOperation(1, "Пользуясь случаем хочу передать привет своей маме!", {from: web3.eth.accounts[6], gas: 800000});

  var Auto = web3.eth.contract(config.autoAbi).at(a2);

  Auto.makeOperation(1, "Великолепный клиент, всегда идет на встречу. Постоянно предоставляю ему скидку)", {from: web3.eth.accounts[5], gas: 800000});
  Auto.makeOperation(1, "Мороз и солнце; день чудесный!..", {from: web3.eth.accounts[6], gas: 800000});

  Auto.makePrivate({from: web3.eth.accounts[9], gas: 500000});
}
