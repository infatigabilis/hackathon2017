import config from '../config';

export default (web3, name, address, callback) => {
  var Registry = web3.eth.contract(config.registryAbi).new(
    name,
   {
     from: address,
     data: config.registryData,
     gas: config.gas
   }, function (e, contract) {
     if (contract.address != undefined) callback(contract.address);
  });
}
