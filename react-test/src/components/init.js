import config from '../config';
import createRegistry from './createRegistry';
import createAuto from './createAuto';

export default (web3, callback) => {
  createRegistry(web3, "Федеральный реестр", web3.eth.accounts[0], r1 => {
    createRegistry(web3, "Реестр 'Радуга'", web3.eth.accounts[1], r2 => {
      createAuto(web3, r1, "серия_abcd1234", web3.eth.accounts[2], a1 => {
        createAuto(web3, r2, "id_AB693DNLC0", web3.eth.accounts[9], a2 => {
          callback(r1, r2, a1, a2);
        });
      });
    });
  });
}
