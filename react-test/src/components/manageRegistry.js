import config from '../config';

export default (web3, r1, r2) => {
  var Registry = web3.eth.contract(config.registryAbi).at(r1);

  Registry.touch({from: web3.eth.accounts[0]});

  Registry.addPerformer(web3.eth.accounts[4], "СТО 'Отец и сыновья'", 100, "Починим все!", {from: web3.eth.accounts[0], gas: 500000});
  Registry.addPerformer(web3.eth.accounts[5], "Автомойка 'Форсаж'", 30, "http://site.com", {from: web3.eth.accounts[0], gas: 500000});

  Registry.addOperationType("Замена масла", "...", {from: web3.eth.accounts[0], gas: 500000});
  Registry.addOperationType("Диагностика", "Быстро и качественно!", {from: web3.eth.accounts[0], gas: 500000});

  var Registry = web3.eth.contract(config.registryAbi).at(r2);

  Registry.touch({from: web3.eth.accounts[1]});

  Registry.addPerformer(web3.eth.accounts[5], "Иванов Иван Иванович", 10, "Приятные цены, качественное обслуживание, индивидуальный подход", {from: web3.eth.accounts[1], gas: 500000});
  Registry.addPerformer(web3.eth.accounts[6], "Межгалактическое Общество Демиургов", 90, "http://site.com", {from: web3.eth.accounts[1], gas: 500000});

  Registry.addOperationType("Cупер замена масла", "http://web.com", {from: web3.eth.accounts[1], gas: 500000});
}
