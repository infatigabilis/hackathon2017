import React, { Component } from 'react';
import Web3 from 'web3';
import init from './components/init';
import config from './config';
import manageRegistry from './components/manageRegistry';
import manageAuto from './components/manageAuto';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Informer from './ui/Informer';

class App extends Component {
  render() {
    var web3 = new Web3(new Web3.providers.HttpProvider(config.host));

    init(web3, (r1, r2, a1, a2) => {
      manageRegistry(web3, r1, r2)
      manageAuto(web3, a1, a2, r2);
      console.log(r1);
      console.log(r2);
      console.log(a1);
      console.log(a2);
    })

    return (
      <MuiThemeProvider>
        <Informer />
      </MuiThemeProvider>
    );
  }
}

export default App;
